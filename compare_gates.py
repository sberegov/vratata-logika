#!/bin/python3

from sympy import *
DL, DU, DC, NL, BL, NU, BU, KU, EU, DDL, DDU, DDC, DBL, DBU, ES, ER, EES, EER, UNS, UNR, LNS, LNR, UE, LE, DWLS, DWLR, EG, LG, UG, T1C, T2C, T3C, T4C, CB0, CB1, CB2, CB3, CB4, CB5, CCK, CCLR, E, UN, LN, DWL, EE, T1OVF, T2OVF, T3OVF, SB, SBS, SBR, T5C, T5OVF = \
symbols('DL DU DC NL BL NU BU KU EU DDL DDU DDC DBL DBU ES ER EES EER UNS UNR LNS LNR UE LE DWLS DWLR EG LG UG T1C T2C T3C T4C CB0 CB1 CB2 CB3 CB4 CB5 CCK CCLR E UN LN DWL EE T1OVF T2OVF T3OVF SB SBS SBR T5C T5OVF')

#UNS =
print((DBU & (~ LG) & (~ EG) & (~ DDU) | (LG & ~ DDC)).equals(~(~ (((DBU & ~ LG) & ~ EG) & ~ DDU) & ~(LG & ~ DDC))))
#UNR =
print((DDU | EG).equals(~(~ (DDU | EG))))
#LNS =
print((DWL & (~ UG) & (~ EG) & (~ DDL) & DDC).equals( ((((DWL & ~ UG) & ~ EG) & ~ DDL) & DDC) ))
#LNR =
print((DDL | EG | (~ DDC & LG)).equals( ~((~DDL & ~EG) & ~(~DDC & LG)) ))
#DWLS =
print((DBL & (~ DDL) & (~ UN)).equals( ((~DDL & ~UN) & DBL) ))
#DWLR =
print((DBU | LNS | T1OVF).equals( ~((~DBU & ~LNS) & ~T1OVF) ))
#ES =
print((T2OVF | (DDU & DDL)).equals( ~(~T2OVF & ~(DDU & DDL)) ))
#EES =
print((T3OVF | ((E & ((DDU & EU) | (DDL & ~ EU))) & ~ (DDU & DDL))).equals( ~( ~T3OVF & ~((~(~(DDL & ~EU) & ~(DDU & EU)) & E) & ~(DDU & DDL)) ) ))
#UG =
print((( (UN & (~ E)) | (UE & E) ) & (~ EE)).equals( (~EE & ~(~(UE & E) & ~(UN & ~E))) ))
#LG =
print((( (LN & (~ E)) | (LE & E) ) & (~ EE)).equals( (~EE & ~(~(LE & E) & ~(LN & ~E))) ))
#T2C =
print(((LN | UN)).equals( ~(~(LN | UN)) ))
#T4C =
print((E & (~ EE)).equals( (E & ~EE) ))
#UE =
print((CB0 & ( (~ CB4 & EU) | (CB4 & ~ EU) )).equals( (CB0 & ~(~(~CB4 & EU) & ~(CB4 & ~EU))) ))
#LE =
print((CB0 & ( (CB4 & EU) | (~ CB4 & ~ EU) )).equals( (CB0 & ~(~(CB4 & EU) & ~(~CB4 & ~EU))) ))
#SBS =
print(  (DBU & DDU).equals( ~( ~(DBU & DDU) ) ) )
#SBR =
print(  (T5OVF | DBL | ~DDU).equals( ~((~T5OVF & ~DBL) & DDU) ) )
