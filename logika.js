"use strict";

class Timer{
	constructor(timer_class, duration){
		this.timer_class = timer_class;
		this.duration = duration;
		this.elapsed = 0;
		this.counting = false;
		this.ovf = false;
	}
	reset(){
		changeTimerProgress(this.timer_class, 0);
		this.elapsed = 0;
	}
	start(){
		this.counting = true;
	}
	stop(){
		this.counting = false;
	}
	advance(time){
		if(this.counting){
			this.elapsed += time;
			if(this.elapsed > this.duration){
				this.ovf = true;
				this.elapsed = 0;
			}
			let value = parseInt(this.elapsed/this.duration*100)
			changeTimerProgress(this.timer_class, value);
		}
	}
	ovf(){
		return this.ovf;
	}
	clear(){
		this.ovf = false;
	}
}

class InternalSignal{ // Signals mapped to hardware
	constructor(signal_class){
		this.signal_class = signal_class;
		this.state = false;
	}
	update(){setState(this.signal_class, this.state);}
	w(state){this.state=state;}
	r(){return this.state;}

}

class TRST_Signal extends InternalSignal{ //input
	constructor(timer){
		super(timer.timer_class + 'RST');
		this.timer = timer;
	}
	update(){
		if(this.state)this.timer.reset();
		super.update();
	}
//	w(state){}
//	r(){}
}
class TSTT_Signal extends InternalSignal{ //input
	constructor(timer){
		super(timer.timer_class + 'STT');
		this.timer = timer;
	}
	update(){
		if(this.state)this.timer.start();
		super.update();
	}
//	w(state){}
//	r(){}
}
class TSTP_Signal extends InternalSignal{ //input
	constructor(timer){
		super(timer.timer_class + 'STP');
		this.timer = timer;
	}
	update(){
		if(this.state)this.timer.stop();
		super.update();
	}
//	w(state){}
//	r(){}
}
class TOVF_Signal extends InternalSignal{ //output
	constructor(timer){
		super(timer.timer_class + 'OVF');
		this.timer = timer;
	}
	update(){
		this.state = this.timer.ovf;
		super.update();
	}
	w(state){} // read-only
	r(){
		this.timer.clear(); // only a single impulse
		return super.r();
	}
}

class CBn_Signal extends InternalSignal{ //output
	constructor(counter, n){
		super(counter + 'B' + n);
		this.counter = counter;
		this.n = n;
	}
	update(){
		var num = getCounter(this.counter);
		this.state = num & (1<<this.n) ? 1 : 0;
		super.update();
	}
	w(state){} // read-only
//	r(){}
}
class CCK_Signal extends InternalSignal{ //input
	constructor(counter){
		super(counter + 'CK');
		this.counter = counter;
	}
//	update(){}
	w(state){
		if(state)incrementCounter(this.counter);
		this.state = state;
	}
//	r(){}
}
class CCLR_Signal extends InternalSignal{ //input
	constructor(counter){
		super(counter + 'CLR');
		this.counter = counter;
	}
//	update(){}
	w(state){
		if(state)clearCounter(this.counter);
		this.state = state;
	}
//	r(){}
}

class RS_Signal extends InternalSignal{ //output
	constructor(rs_class){
		super(rs_class);
	}
	update(){
		let set_signal = getState(this.signal_class + 'S');
		let reset_signal = getState(this.signal_class + 'R');
		if(set_signal && reset_signal)this.state = true;
		else if(set_signal)this.state = true;
		else if(reset_signal)this.state = false;
		super.update();
	}
	w(state){} // read-only
//	r(){}

}

function extendedSetState(signal_class, state){
	if(internal_signal_classes.includes(signal_class)){ //internal signal
		let intsig = internal_signals[internal_signal_classes.indexOf(signal_class)];
		intsig.w(state);
	}
	else{
		setState(signal_class, state);
	}
}

function extendedGetState(signal_class){
	if(internal_signal_classes.includes(signal_class)){ //internal signal
		let intsig = internal_signals[internal_signal_classes.indexOf(signal_class)];
		return intsig.r();
	}
	else{
		return getState(signal_class);
	}
}

var sig_links = {};
var equations = {};

function addEquation(equation){
	const node = math.parse(equation);
	const filtered = node.filter(function (node) {
  		return node.isSymbolNode
	});
	let sig_set = ''
	let sig_list = []
	for(const node_num in filtered){
		let sig = filtered[node_num].name;
		if(node_num == 0){ //eq1
			sig_set = sig;
		}
		else{ //eq2
			sig_list.push(sig);
		}
	}
	sig_links[sig_set] = sig_list;
	const code = node.compile();
	equations[sig_set] = code;
}

var scope = {}

function ssHelper(signals, signal_name){
	signals[signal_name] = extendedGetState(signal_name);
}

function signalStates(){
	let signals = {}
	let signal_names = [];
	for(const node of document.querySelectorAll('.input, .boolean')){
		signal_names.push(node.classList[0]);
	}
	for(const name of signal_names){
		ssHelper(signals, name);
	}
	return signals; //scope
}

function updateLinks(){
	for(const link in sig_links){
		let signals = {}
		for(const signal of sig_links[link]){
			signals[signal] = scope[signal]
		}
		setBooleanSignals(link, signals);
	}
}

function internalSignalClasses(internal_signals){
	internal_signal_classes = []
	for(const sig of internal_signals){
		internal_signal_classes.push(sig.signal_class)
	}
	return internal_signal_classes;
}

var input_inverter = '';

function processInputClick(type, iclass){
	if(type == 'md'){
		input_inverter = iclass;
	}
	else{
		input_inverter = '';
	}
}

class InputSignal{ // Non-deterministic bistable logic
	constructor(input_class, logic, initial_state){
		this.logic = logic;
		this.iclass = input_class;
		this.state = initial_state;
	}

	chst(state){
		setState(this.iclass, state);
	}

	update(mod = false){
		let [newstate, change] = this.logic();
		if(!change) newstate = this.state; // bistable
		else this.state = newstate;
		let radio_state = getInputRadio(this.iclass);
		let state = false;
		switch(radio_state){
			case 'in_hi':
				state = true;
			break;
			case 'in_lo':
				state = false;
			break;
			case 'in_no':
				state = newstate;
			break;
		}
		if(input_inverter == this.iclass) state = !state;
		if(!mod) this.chst(state);
		else return state;
	}

}

class PulsedInputSignal{
	constructor(input_signal){
		this.isignal = input_signal;
		this.sig_state = false;
	}

	update(){
		let newstate = this.isignal.update(true);
		let state = false;
		if(this.sig_state && !newstate) state = true; // neg impulse
		this.sig_state = newstate;
		this.isignal.chst(state);
	}
}

var pins_position = 0.0;
function extendPins(amount){
	pins_position += amount;
	if(pins_position >= 1){
		pins_position = 1;
		return true;
	}
	return false;
}
function retractPins(amount){
	pins_position -= amount;
	if(pins_position <= 0){
		pins_position = 0;
		return true;
	}
	return false;
}

function pinRandom(lock){
	if(typeof pinRandom.last_time_l == 'undefined'){
		pinRandom.last_time_l = Date.now();
	}
	if(typeof pinRandom.last_time_u == 'undefined'){
		pinRandom.last_time_u = Date.now();
	}
	let tel = 0;
	if(lock){
		tel = Date.now() - pinRandom.last_time_l;
		pinRandom.last_time_l = Date.now();
	}
	else{
		tel = Date.now() - pinRandom.last_time_u;
		pinRandom.last_time_u = Date.now();
	}
	let rnd = Math.random()*0.33 + 0.66;
	// target is L in > 2 seconds.
	return rnd/(2000/tel);
}

function DLLogic(){
	let amount = pinRandom(true);
	let lgs = getState('LG');
	let res = false;
	if(lgs)res = extendPins(amount);
	else res = extendPins(0);
	if(res){
		return [true, true]; // Set input to HIGH
	}
	else{
		return [false, true]; // Set input to LOW
	}
//	}
//	else return [false, false]; // Do not set input
}

function DULogic(){
	let amount = pinRandom(false);
	let ugs = getState('UG');
	let res = false;
	if(ugs) res = retractPins(amount);
	else res = retractPins(0);
	if(res){
		return [true, true]; // Set input to HIGH
	}
	else{
		return [false, true]; // Set input to LOW
	}
//	}
//	else return [false, false]; // Do not set input
}

var DL = new InputSignal('DL', DLLogic, false);
var DU = new InputSignal('DU', DULogic, true);
var DC = new InputSignal('DC', function(){ return [false, false]; }, true);
var EU = new InputSignal('EU', function(){ return [false, false]; }, false);
var NL = new PulsedInputSignal(new InputSignal('NL', function(){ return [false, false]; }, false));
var BL = new PulsedInputSignal(new InputSignal('BL', function(){ return [false, false]; }, false));
var NU = new PulsedInputSignal(new InputSignal('NU', function(){ return [false, false]; }, false));
var BU = new PulsedInputSignal(new InputSignal('BU', function(){ return [false, false]; }, false));
var KU = new PulsedInputSignal(new InputSignal('KU', function(){ return [false, false]; }, false));

var T1 = new Timer('T1', 60000);
var T2 = new Timer('T2', 5000);
var T3 = new Timer('T3', 600000);
var T4 = new Timer('T4', 500);
var T5 = new Timer('T5', 1000);
var T1RST = new TRST_Signal(T1);
var T1STT = new TSTT_Signal(T1);
var T1STP = new TSTP_Signal(T1);
var T1OVF = new TOVF_Signal(T1);
var T2RST = new TRST_Signal(T2);
var T2STT = new TSTT_Signal(T2);
var T2STP = new TSTP_Signal(T2);
var T2OVF = new TOVF_Signal(T2);
var T3RST = new TRST_Signal(T3);
var T3STT = new TSTT_Signal(T3);
var T3STP = new TSTP_Signal(T3);
var T3OVF = new TOVF_Signal(T3);
var T4RST = new TRST_Signal(T4);
var T4STT = new TSTT_Signal(T4);
var T4STP = new TSTP_Signal(T4);
var T4OVF = new TOVF_Signal(T4);
var T5RST = new TRST_Signal(T5);
var T5STT = new TSTT_Signal(T5);
var T5STP = new TSTP_Signal(T5);
var T5OVF = new TOVF_Signal(T5);
var CB0 = new CBn_Signal('C', 0);
var CB1 = new CBn_Signal('C', 1);
var CB2 = new CBn_Signal('C', 2);
var CB3 = new CBn_Signal('C', 3);
var CB4 = new CBn_Signal('C', 4);
var CB5 = new CBn_Signal('C', 5);
var CCK = new CCK_Signal('C');
var CCLR = new CCLR_Signal('C');
var E = new RS_Signal('E');
var UN = new RS_Signal('UN');
var LN = new RS_Signal('LN');
var DWL = new RS_Signal('DWL');
var EE = new RS_Signal('EE');
var SB = new RS_Signal('SB');

var timers = [T1, T2, T3, T4, T5];

var internal_signals =
	[T1RST, T1STT, T1STP, T1OVF,
	 T2RST, T2STT, T2STP, T2OVF,
	 T3RST, T3STT, T3STP, T3OVF,
	 T4RST, T4STT, T4STP, T4OVF,
	 T5RST, T5STT, T5STP, T5OVF,
	 CB0, CB1, CB2, CB3, CB4, CB5, CCK, CCLR,
	 E, UN, LN, DWL, EE, SB];

var internal_signal_classes = internalSignalClasses(internal_signals);

var input_signals = [DL, DU, DC, NL, BL, NU, BU, KU, EU];

var loop_timer;
function door_loop(){
	let tel = timeElapsed();
	for(const tmr of timers){
		tmr.advance(tel);
	}
	for(const sig of internal_signals){
		sig.update();
	}
	scope = signalStates();
	updateLinks();
	for(const eq in equations){
		let result = equations[eq].evaluate(scope);
		extendedSetState(eq, result);
	}
	for(const isig of input_signals){
		isig.update();
	}
}

loop_timer = setInterval(door_loop, 50); // !!! reduce for precision, ms

function timeElapsed(){
	if( typeof timeElapsed.time == 'undefined' ) {
		timeElapsed.time = Date.now();
	}
	var te = Date.now() - timeElapsed.time;
	timeElapsed.time = Date.now();
	return te;
}

function logicEquations(){
	addEquation("DBU = (NU or BU or KU)");
	addEquation("DBL = (NL or BL)");
	addEquation("DDU = DU");
	addEquation("DDL = DL");
	addEquation("DDC = DC");
	addEquation("EG = E");
	addEquation("UNS = DBU and (not LG) and (not EG) and (not DDU) or (LG and not DDC)");
	addEquation("UNR = DDU or EG");
	addEquation("LNS = DWL and (not UG) and (not EG) and (not DDL) and DDC");
	addEquation("LNR = DDL or EG or (not DDC and LG)");
	addEquation("DWLS = DBL and (not DDL) and (not UN)");
	addEquation("DWLR = DBU or LNS or T1OVF");
	addEquation("ES = T2OVF or (DDU and DDL)");
	addEquation("ER = 0");
	addEquation("EES = T3OVF or ((E and ((DDU and EU) or (DDL and not EU))) and not (DDU and DDL))");
	addEquation("EER = 0");
	addEquation("UG = ( (UN and (not E)) or (UE and E) ) and (not EE)");
	addEquation("LG = ( (LN and (not E)) or (LE and E) ) and (not EE)");
	addEquation("T1STT = DWL");
	addEquation("T1STP = not DWL");
	addEquation("T1RST = not DWL");
	addEquation("T2STT = (LN or UN)");
	addEquation("T2STP = not (LN or UN)");
	addEquation("T2RST = not (LN or UN)");
	addEquation("T3STT = E");
	addEquation("T3STP = not E");
	addEquation("T3RST = not E");
	addEquation("T4STT = E and (not EE)");
	addEquation("T4STP = EE or (not E)");
	addEquation("T4RST = EE or (not E)");
	addEquation("CCK = T4OVF");
	addEquation("UE = CB0 and ( ((not CB4) and EU) or (CB4 and (not EU)) )");
	addEquation("LE = CB0 and ( (CB4 and EU) or ((not CB4) and (not EU)) )");
	addEquation("CCLR = CB1 and CB4");
	addEquation("SBS = DBU and DDU");
	addEquation("SBR = T5OVF or DBL or not DDU");
	addEquation("T5STT = SB");
	addEquation("T5STP = not SB");
	addEquation("T5RST = not SB");
	addEquation("SBG = SB");
//	addEquation("");

}
