{::options parse_block_html="true" /}

<head>
<link rel="stylesheet" href="vrata.css">
<script src="math.js"></script>
<script src="vrata.js"></script>
<script src="logika.js"></script>
</head>

<div class="diagram">

<div class="DL input">
# DL
DoorLocked - HIGH when the lock pins are fully extracted
</div>

<div class="DU input">
# DU
DoorUnlocked - HIGH when the lock pins are fully retracted
</div>

<div class="DC input">
# DC
DoorClosed - HIGH when the door is closed
</div>

<div class="NL input">
# NL
NeomontanaLockSignal - 10ms IMPULSE initiates door locking 
</div>

<div class="BL input">
# BL
ButtonLockSignal - 10ms IMPULSE initiates door locking 
</div>

<div class="NU input">
# NU
NeomontanaUnlockSignal - 10ms IMPULSE initiates door unlocking 
</div>

<div class="BU input">
# BU
ButtonUnlockSignal - 10ms IMPULSE initiates door unlocking 
</div>

<div class="KU input">
# KU
KeyUnlockSignal - 10ms IMPULSE initiates door unlocking 
</div>

<div class="EU input">
# EU
Set emergency mode locking OR unlocking 
</div>

<div class="T1 timer">
# T1
Timer 1 - 60s door close timeout 
</div>

<div class="T2 timer">
# T2
Timer 2 - 5s lock pins actuation timeout 
</div>

<div class="T3 timer">
# T3
Timer 3 - 600s emergency lock/unlock timer 
</div>

<div class="T4 timer">
# T4
Timer 4 - 0.5s emergency pin reversal timer 
</div>

<div class="T5 timer">
# T5
Timer 5 - 1s strike body timer
</div>

<div class="DDL boolean output">
# DDL
DebouncedDoorLockedSignal 
</div>

<div class="DDU boolean output">
# DDU
DebouncedDoorUnlockedSignal
</div>

<div class="DDC boolean output">
# DDC
DebouncedDoorClosedSignal
</div>

<div class="DBL boolean">
# DBL
DebouncedLockSignal
</div>

<div class="DBU boolean">
# DBU
DebouncedUnlockSignal
</div>

<div class="ES boolean">
# ES
EmergencySignalSet - coils in parallel 
</div>

<div class="ER boolean">
# ER
EmergencySignalReset - coils in parallel 
</div>

<div class="EES boolean">
# EES
EmergencyEndSet - stop trying to {EU} 
</div>

<div class="EER boolean">
# EER
EmergencyEndReset - nil 
</div>

<div class="UNS boolean">
# UNS
UnlockNormalSet 
</div>

<div class="UNR boolean">
# UNR
UnlockNormalReset 
</div>

<div class="LNS boolean">
# LNS
LockNormalSet 
</div>

<div class="LNR boolean">
# LNR
LockNormalReset 
</div>

<div class="UE boolean">
# UE
UnlockEmergency 
</div>

<!--<div class="UER boolean">-->
<!--# UER-->
<!--UnlockEmergencyReset -->
<!--</div>-->

<div class="LE boolean">
# LE
LockEmeregncy 
</div>

<!--<div class="LER boolean">-->
<!--# LER-->
<!--LockEmergencyReset -->
<!--</div>-->

<div class="DWLS boolean">
# DWLS
DoorWaitLockSet
</div>

<div class="DWLR boolean">
# DWLR
DoorWaitLockReset
</div>

<div class="C counter">
# C
SIPO - emergency mode shift register 
</div>

<div class="EG boolean output">
# EG
EmergencyGate 
</div>

<div class="LG boolean output">
# LG
LockGate 
</div>

<div class="UG boolean output">
# UG
UnlockGate 
</div>

<div class="SBG boolean output">
# SBG
StrikeBodyGate 
</div>

<div class="SBS boolean">
# SBS
StrikeBodySet 
</div>

<div class="SBR boolean">
# SBR
StrikeBodyReset 
</div>

<div class="T1RST boolean internal">
# T1RST
Timer1Reset 
</div>

<div class="T1STT boolean internal">
# T1STT
Timer1Start 
</div>

<div class="T1STP boolean internal">
# T1STP
Timer1Stop 
</div>

<div class="T1OVF boolean internal">
# T1OVF
Timer1Overflow 
</div>

<!--<div class="T1CLR boolean internal">-->
<!--# T1CLR-->
<!--Timer1Clear -->
<!--</div>-->

<div class="T2RST boolean internal">
# T2RST
Timer2Reset 
</div>

<div class="T2STT boolean internal">
# T2STT
Timer2Start 
</div>

<div class="T2STP boolean internal">
# T2STP
Timer2Stop 
</div>

<div class="T2OVF boolean internal">
# T2OVF
Timer2Overflow 
</div>

<!--<div class="T2CLR boolean internal">-->
<!--# T2CLR-->
<!--Timer2Clear -->
<!--</div>-->

<div class="T3RST boolean internal">
# T3RST
Timer3Reset 
</div>

<div class="T3STT boolean internal">
# T3STT
Timer3Start 
</div>

<div class="T3STP boolean internal">
# T3STP
Timer3Stop 
</div>

<div class="T3OVF boolean internal">
# T3OVF
Timer3Overflow 
</div>

<!--<div class="T3CLR boolean internal">-->
<!--# T3CLR-->
<!--Timer3Clear -->
<!--</div>-->

<div class="T4RST boolean internal">
# T4RST
Timer4Reset 
</div>

<div class="T4STT boolean internal">
# T4STT
Timer4Start 
</div>

<div class="T4STP boolean internal">
# T4STP
Timer4Stop 
</div>

<div class="T4OVF boolean internal">
# T4OVF
Timer4Overflow 
</div>

<!--<div class="T4CLR boolean internal">-->
<!--# T4CLR-->
<!--Timer4Clear -->
<!--</div>-->

<div class="T5RST boolean internal">
# T5RST
Timer5Reset 
</div>

<div class="T5STT boolean internal">
# T5STT
Timer5Start 
</div>

<div class="T5STP boolean internal">
# T5STP
Timer5Stop 
</div>

<div class="T5OVF boolean internal">
# T5OVF
Timer5Overflow 
</div>

<!--<div class="T5CLR boolean internal">-->
<!--# T5CLR-->
<!--Timer5Clear -->
<!--</div>-->

<div class="CB0 boolean internal">
# CB0
CounterBit0 
</div>

<div class="CB1 boolean internal">
# CB1
CounterBit1 
</div>

<div class="CB2 boolean internal">
# CB2
CounterBit2 
</div>

<div class="CB3 boolean internal">
# CB3
CounterBit3 
</div>

<div class="CB4 boolean internal">
# CB4
CounterBit4 
</div>

<div class="CB5 boolean internal">
# CB5
CounterBit5 
</div>

<div class="CCK boolean internal">
# CCK
CounterClock 
</div>

<div class="CCLR boolean internal">
# CCLR
CounterClear 
</div>

<div class="E boolean internal">
# E
Emergency - RS trigger 
</div>

<div class="UN boolean internal">
# UN
UnlockNormalMode - RS trigger
</div>

<div class="LN boolean internal">
# LN
LockNormalMode - RS trigger 
</div>

<div class="DWL boolean internal">
# DWL
DoorWaitLock - RS trigger 
</div>

<div class="EE boolean internal">
# EE
EmergencyEnd - RS trigger 
</div>

<div class="SB boolean internal">
# SB
StrikeBody - RS trigger
</div>

</div>
