EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC02 U?
U 4 1 675C8291
P 1050 2550
AR Path="/675C8291" Ref="U?"  Part="4" 
AR Path="/671E6DCB/675C8291" Ref="U15"  Part="4" 
F 0 "U15" H 1050 2800 24  0000 C CNN
F 1 "74HC02" H 1050 2750 24  0000 C CNN
F 2 "" H 1050 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc02" H 1050 2550 50  0001 C CNN
	4    1050 2550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U?
U 4 1 675C8297
P 2450 2550
AR Path="/675C8297" Ref="U?"  Part="6" 
AR Path="/671E6DCB/675C8297" Ref="U16"  Part="4" 
F 0 "U16" H 2450 2750 24  0000 C CNN
F 1 "74HC04" H 2450 2700 24  0000 C CNN
F 2 "" H 2450 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2450 2550 50  0001 C CNN
	4    2450 2550
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 2 1 678487C0
P 1950 1200
AR Path="/678487C0" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678487C0" Ref="U16"  Part="2" 
F 0 "U16" H 1950 1400 24  0000 C CNN
F 1 "74HC04" H 1950 1350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1950 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1950 1200 50  0001 C CNN
	2    1950 1200
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 6 1 6784AC63
P 6100 7200
AR Path="/6784AC63" Ref="U?"  Part="1" 
AR Path="/671E6DCB/6784AC63" Ref="U19"  Part="6" 
F 0 "U19" H 6100 7400 24  0000 C CNN
F 1 "74HC04" H 6100 7350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6100 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6100 7200 50  0001 C CNN
	6    6100 7200
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 5 1 6784F138
P 5650 7200
AR Path="/6784F138" Ref="U?"  Part="1" 
AR Path="/671E6DCB/6784F138" Ref="U19"  Part="5" 
F 0 "U19" H 5650 7400 24  0000 C CNN
F 1 "74HC04" H 5650 7350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5650 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5650 7200 50  0001 C CNN
	5    5650 7200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 67877D01
P 1250 5000
AR Path="/67877D01" Ref="C?"  Part="1" 
AR Path="/671E6DCB/67877D01" Ref="C30"  Part="1" 
F 0 "C30" V 1050 5000 24  0000 C CNN
F 1 "100n" V 1100 5000 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1250 5000 50  0001 C CNN
F 3 "~" H 1250 5000 50  0001 C CNN
	1    1250 5000
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 7 1 678530C7
P 1250 4850
AR Path="/678530C7" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678530C7" Ref="U16"  Part="7" 
F 0 "U16" V 1150 4850 24  0000 C CNN
F 1 "74HC04" V 1100 4850 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1250 4850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1250 4850 50  0001 C CNN
	7    1250 4850
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 67877D07
P 1750 4850
AR Path="/67877D07" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/67877D07" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 1750 4700 50  0001 C CNN
F 1 "+5V" H 1765 5023 50  0000 C CNN
F 2 "" H 1750 4850 50  0001 C CNN
F 3 "" H 1750 4850 50  0001 C CNN
	1    1750 4850
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 67877D0D
P 750 4800
AR Path="/67877D0D" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/67877D0D" Ref="#GND068"  Part="1" 
F 0 "#GND068" H 750 4700 50  0001 C CNN
F 1 "0" H 750 4889 50  0000 C CNN
F 2 "" H 750 4800 50  0001 C CNN
F 3 "~" H 750 4800 50  0001 C CNN
	1    750  4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 5000 750  5000
Wire Wire Line
	750  5000 750  4850
Wire Wire Line
	1350 5000 1750 5000
Wire Wire Line
	1750 5000 1750 4850
Connection ~ 1750 4850
Wire Wire Line
	750  4850 750  4800
Connection ~ 750  4850
$Comp
L Custom:74HC08 U?
U 1 1 678D9365
P 1050 800
AR Path="/678D9365" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678D9365" Ref="U7"  Part="1" 
F 0 "U7" H 1000 1050 24  0000 C CNN
F 1 "74HC08" H 1000 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1050 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1050 800 50  0001 C CNN
	1    1050 800 
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 2 1 678DAC79
P 1650 800
AR Path="/678DAC79" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678DAC79" Ref="U7"  Part="2" 
F 0 "U7" H 1600 1050 24  0000 C CNN
F 1 "74HC08" H 1600 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1650 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1650 800 50  0001 C CNN
	2    1650 800 
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 4 1 678DC3FC
P 4400 800
AR Path="/678DC3FC" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678DC3FC" Ref="U7"  Part="4" 
F 0 "U7" H 4350 1050 24  0000 C CNN
F 1 "74HC08" H 4350 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4400 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 4400 800 50  0001 C CNN
	4    4400 800 
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 5 1 678E3BC5
P 2450 5450
AR Path="/678E3BC5" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678E3BC5" Ref="U8"  Part="5" 
F 0 "U8" V 2350 5450 24  0000 C CNN
F 1 "74HC08" V 2300 5450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2450 5450 50  0001 C CNN
	5    2450 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 678F91A6
P 2950 5450
AR Path="/678F91A6" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/678F91A6" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 2950 5300 50  0001 C CNN
F 1 "+5V" H 2965 5623 50  0000 C CNN
F 2 "" H 2950 5450 50  0001 C CNN
F 3 "" H 2950 5450 50  0001 C CNN
	1    2950 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 678F9DC3
P 1950 5400
AR Path="/678F9DC3" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/678F9DC3" Ref="#GND069"  Part="1" 
F 0 "#GND069" H 1950 5300 50  0001 C CNN
F 1 "0" H 1950 5489 50  0000 C CNN
F 2 "" H 1950 5400 50  0001 C CNN
F 3 "~" H 1950 5400 50  0001 C CNN
	1    1950 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 678FD752
P 2450 5600
AR Path="/678FD752" Ref="C?"  Part="1" 
AR Path="/671E6DCB/678FD752" Ref="C31"  Part="1" 
F 0 "C31" V 2250 5600 24  0000 C CNN
F 1 "100n" V 2300 5600 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2450 5600 50  0001 C CNN
F 3 "~" H 2450 5600 50  0001 C CNN
	1    2450 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 5600 1950 5600
Wire Wire Line
	2550 5600 2950 5600
Wire Wire Line
	2950 5600 2950 5450
Wire Wire Line
	1950 5400 1950 5450
Wire Wire Line
	1950 5450 1950 5600
Connection ~ 1950 5450
Connection ~ 2950 5450
$Comp
L 74xx:74HC00 U2
U 1 1 6793C4BE
P 7600 1500
F 0 "U2" H 7600 1750 24  0000 C CNN
F 1 "74HC00" H 7600 1700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7600 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 7600 1500 50  0001 C CNN
	1    7600 1500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 2 1 67943466
P 8400 1500
F 0 "U2" H 8400 1750 24  0000 C CNN
F 1 "74HC00" H 8400 1700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8400 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 1500 50  0001 C CNN
	2    8400 1500
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HC00 U3
U 2 1 6794588D
P 9850 1200
F 0 "U3" H 9850 1450 24  0000 C CNN
F 1 "74HC00" H 9850 1400 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9850 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 9850 1200 50  0001 C CNN
	2    9850 1200
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC00 U2
U 4 1 67946C07
P 9450 1500
F 0 "U2" H 9450 1750 24  0000 C CNN
F 1 "74HC00" H 9450 1700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9450 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 9450 1500 50  0001 C CNN
	4    9450 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 679590AB
P 3650 6200
AR Path="/679590AB" Ref="C?"  Part="1" 
AR Path="/671E6DCB/679590AB" Ref="C37"  Part="1" 
F 0 "C37" V 3450 6200 24  0000 C CNN
F 1 "100n" V 3500 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3650 6200 50  0001 C CNN
F 3 "~" H 3650 6200 50  0001 C CNN
	1    3650 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 6795B49C
P 3150 6000
AR Path="/6795B49C" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/6795B49C" Ref="#GND076"  Part="1" 
F 0 "#GND076" H 3150 5900 50  0001 C CNN
F 1 "0" H 3150 6089 50  0000 C CNN
F 2 "" H 3150 6000 50  0001 C CNN
F 3 "~" H 3150 6000 50  0001 C CNN
	1    3150 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 6200 3550 6200
Wire Wire Line
	3750 6200 4150 6200
$Comp
L 74xx:74HC00 U3
U 5 1 6794ED1D
P 3650 6050
F 0 "U3" V 3500 6050 24  0000 C CNN
F 1 "74HC00" V 3550 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3650 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 3650 6050 50  0001 C CNN
	5    3650 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 6795A334
P 4150 6050
AR Path="/6795A334" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/6795A334" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 4150 5900 50  0001 C CNN
F 1 "+5V" H 4165 6223 50  0000 C CNN
F 2 "" H 4150 6050 50  0001 C CNN
F 3 "" H 4150 6050 50  0001 C CNN
	1    4150 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 6200 4150 6050
Connection ~ 4150 6050
Wire Wire Line
	3150 6200 3150 6050
Wire Wire Line
	3150 6000 3150 6050
Connection ~ 3150 6050
$Comp
L 74xx:74HC00 U3
U 1 1 637975EF
P 10250 1500
F 0 "U3" H 10250 1750 24  0000 C CNN
F 1 "74HC00" H 10250 1700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10250 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10250 1500 50  0001 C CNN
	1    10250 1500
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HC00 U3
U 4 1 637975FB
P 8000 2250
F 0 "U3" H 8000 2500 24  0000 C CNN
F 1 "74HC00" H 8000 2450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8000 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8000 2250 50  0001 C CNN
	4    8000 2250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 2 1 63797601
P 8600 2250
F 0 "U4" H 8600 2500 24  0000 C CNN
F 1 "74HC00" H 8600 2450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8600 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8600 2250 50  0001 C CNN
	2    8600 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 63797607
P 4850 6200
AR Path="/63797607" Ref="C?"  Part="1" 
AR Path="/671E6DCB/63797607" Ref="C39"  Part="1" 
F 0 "C39" V 4650 6200 24  0000 C CNN
F 1 "100n" V 4700 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4850 6200 50  0001 C CNN
F 3 "~" H 4850 6200 50  0001 C CNN
	1    4850 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 6379760D
P 4350 6000
AR Path="/6379760D" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/6379760D" Ref="#GND079"  Part="1" 
F 0 "#GND079" H 4350 5900 50  0001 C CNN
F 1 "0" H 4350 6089 50  0000 C CNN
F 2 "" H 4350 6000 50  0001 C CNN
F 3 "~" H 4350 6000 50  0001 C CNN
	1    4350 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 6200 4750 6200
Wire Wire Line
	4950 6200 5350 6200
$Comp
L 74xx:74HC00 U4
U 5 1 63797615
P 4850 6050
F 0 "U4" V 4700 6050 24  0000 C CNN
F 1 "74HC00" V 4750 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4850 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4850 6050 50  0001 C CNN
	5    4850 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 6379761B
P 5350 6050
AR Path="/6379761B" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/6379761B" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 5350 5900 50  0001 C CNN
F 1 "+5V" H 5365 6223 50  0000 C CNN
F 2 "" H 5350 6050 50  0001 C CNN
F 3 "" H 5350 6050 50  0001 C CNN
	1    5350 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 6200 5350 6050
Connection ~ 5350 6050
Wire Wire Line
	4350 6200 4350 6050
Wire Wire Line
	4350 6000 4350 6050
Connection ~ 4350 6050
$Comp
L 74xx:74HC00 U4
U 4 1 6379FDAE
P 9800 2250
F 0 "U4" H 9800 2500 24  0000 C CNN
F 1 "74HC00" H 9800 2450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9800 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 9800 2250 50  0001 C CNN
	4    9800 2250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 1 1 6379FDBA
P 8300 2650
F 0 "U4" H 8300 2900 24  0000 C CNN
F 1 "74HC00" H 8300 2850 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8300 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8300 2650 50  0001 C CNN
	1    8300 2650
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC00 U4
U 3 1 6379FDC0
P 9500 2650
F 0 "U4" H 9500 2900 24  0000 C CNN
F 1 "74HC00" H 9500 2850 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9500 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 9500 2650 50  0001 C CNN
	3    9500 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 6379FDC6
P 6050 6200
AR Path="/6379FDC6" Ref="C?"  Part="1" 
AR Path="/671E6DCB/6379FDC6" Ref="C40"  Part="1" 
F 0 "C40" V 5850 6200 24  0000 C CNN
F 1 "100n" V 5900 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 6050 6200 50  0001 C CNN
F 3 "~" H 6050 6200 50  0001 C CNN
	1    6050 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 6379FDCC
P 5550 6000
AR Path="/6379FDCC" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/6379FDCC" Ref="#GND080"  Part="1" 
F 0 "#GND080" H 5550 5900 50  0001 C CNN
F 1 "0" H 5550 6089 50  0000 C CNN
F 2 "" H 5550 6000 50  0001 C CNN
F 3 "~" H 5550 6000 50  0001 C CNN
	1    5550 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 6200 5950 6200
Wire Wire Line
	6150 6200 6550 6200
$Comp
L 74xx:74HC00 U5
U 5 1 6379FDD4
P 6050 6050
F 0 "U5" V 5900 6050 24  0000 C CNN
F 1 "74HC00" V 5950 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6050 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6050 6050 50  0001 C CNN
	5    6050 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 6379FDDA
P 6550 6050
AR Path="/6379FDDA" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/6379FDDA" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 6550 5900 50  0001 C CNN
F 1 "+5V" H 6565 6223 50  0000 C CNN
F 2 "" H 6550 6050 50  0001 C CNN
F 3 "" H 6550 6050 50  0001 C CNN
	1    6550 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 6200 6550 6050
Connection ~ 6550 6050
Wire Wire Line
	5550 6200 5550 6050
Wire Wire Line
	5550 6000 5550 6050
Connection ~ 5550 6050
$Comp
L 74xx:74HC00 U5
U 4 1 637AB689
P 4550 3400
F 0 "U5" H 4550 3650 24  0000 C CNN
F 1 "74HC00" H 4550 3600 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4550 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4550 3400 50  0001 C CNN
	4    4550 3400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 3 1 637AB68F
P 4250 3800
F 0 "U5" H 4250 4050 24  0000 C CNN
F 1 "74HC00" H 4250 4000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4250 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4250 3800 50  0001 C CNN
	3    4250 3800
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC00 U6
U 1 1 637AB695
P 6250 3500
F 0 "U6" H 6250 3750 24  0000 C CNN
F 1 "74HC00" H 6250 3700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6250 3500 50  0001 C CNN
	1    6250 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 637AB69B
P 7250 6200
AR Path="/637AB69B" Ref="C?"  Part="1" 
AR Path="/671E6DCB/637AB69B" Ref="C41"  Part="1" 
F 0 "C41" V 7050 6200 24  0000 C CNN
F 1 "100n" V 7100 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 7250 6200 50  0001 C CNN
F 3 "~" H 7250 6200 50  0001 C CNN
	1    7250 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 637AB6A1
P 6750 6000
AR Path="/637AB6A1" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/637AB6A1" Ref="#GND081"  Part="1" 
F 0 "#GND081" H 6750 5900 50  0001 C CNN
F 1 "0" H 6750 6089 50  0000 C CNN
F 2 "" H 6750 6000 50  0001 C CNN
F 3 "~" H 6750 6000 50  0001 C CNN
	1    6750 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 6200 7150 6200
Wire Wire Line
	7350 6200 7750 6200
$Comp
L 74xx:74HC00 U6
U 5 1 637AB6A9
P 7250 6050
F 0 "U6" V 7100 6050 24  0000 C CNN
F 1 "74HC00" V 7150 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7250 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 7250 6050 50  0001 C CNN
	5    7250 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 637AB6AF
P 7750 6050
AR Path="/637AB6AF" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/637AB6AF" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 7750 5900 50  0001 C CNN
F 1 "+5V" H 7765 6223 50  0000 C CNN
F 2 "" H 7750 6050 50  0001 C CNN
F 3 "" H 7750 6050 50  0001 C CNN
	1    7750 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 6200 7750 6050
Connection ~ 7750 6050
Wire Wire Line
	6750 6200 6750 6050
Wire Wire Line
	6750 6000 6750 6050
Connection ~ 6750 6050
$Comp
L 74xx:74HC00 U6
U 3 1 637B6F08
P 6850 3500
F 0 "U6" H 6850 3750 24  0000 C CNN
F 1 "74HC00" H 6850 3700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6850 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6850 3500 50  0001 C CNN
	3    6850 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U6
U 2 1 637B6F0E
P 6550 3900
F 0 "U6" H 6550 4150 24  0000 C CNN
F 1 "74HC00" H 6550 4100 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6550 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6550 3900 50  0001 C CNN
	2    6550 3900
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC00 U1
U 4 1 637C973F
P 10150 800
F 0 "U1" H 10150 1050 24  0000 C CNN
F 1 "74HC00" H 10150 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10150 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10150 800 50  0001 C CNN
	4    10150 800 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 3 1 637C9745
P 8000 1200
F 0 "U2" H 8000 1450 24  0000 C CNN
F 1 "74HC00" H 8000 1400 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8000 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8000 1200 50  0001 C CNN
	3    8000 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 637C974B
P 2450 6200
AR Path="/637C974B" Ref="C?"  Part="1" 
AR Path="/671E6DCB/637C974B" Ref="C35"  Part="1" 
F 0 "C35" V 2250 6200 24  0000 C CNN
F 1 "100n" V 2300 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2450 6200 50  0001 C CNN
F 3 "~" H 2450 6200 50  0001 C CNN
	1    2450 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 637C9751
P 1950 6000
AR Path="/637C9751" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/637C9751" Ref="#GND073"  Part="1" 
F 0 "#GND073" H 1950 5900 50  0001 C CNN
F 1 "0" H 1950 6089 50  0000 C CNN
F 2 "" H 1950 6000 50  0001 C CNN
F 3 "~" H 1950 6000 50  0001 C CNN
	1    1950 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 6200 2350 6200
Wire Wire Line
	2550 6200 2950 6200
$Comp
L 74xx:74HC00 U2
U 5 1 637C9759
P 2450 6050
F 0 "U2" V 2300 6050 24  0000 C CNN
F 1 "74HC00" V 2350 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2450 6050 50  0001 C CNN
	5    2450 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 637C975F
P 2950 6050
AR Path="/637C975F" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/637C975F" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 2950 5900 50  0001 C CNN
F 1 "+5V" H 2965 6223 50  0000 C CNN
F 2 "" H 2950 6050 50  0001 C CNN
F 3 "" H 2950 6050 50  0001 C CNN
	1    2950 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 6200 2950 6050
Connection ~ 2950 6050
Wire Wire Line
	1950 6200 1950 6050
Wire Wire Line
	1950 6000 1950 6050
Connection ~ 1950 6050
$Comp
L 74xx:74HC00 U1
U 1 1 637DF186
P 2250 800
F 0 "U1" H 2250 1050 24  0000 C CNN
F 1 "74HC00" H 2250 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2250 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2250 800 50  0001 C CNN
	1    2250 800 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U1
U 2 1 637DF192
P 2550 1200
F 0 "U1" H 2550 1450 24  0000 C CNN
F 1 "74HC00" H 2550 1400 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2550 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2550 1200 50  0001 C CNN
	2    2550 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 637DF19E
P 1250 6200
AR Path="/637DF19E" Ref="C?"  Part="1" 
AR Path="/671E6DCB/637DF19E" Ref="C32"  Part="1" 
F 0 "C32" V 1050 6200 24  0000 C CNN
F 1 "100n" V 1100 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1250 6200 50  0001 C CNN
F 3 "~" H 1250 6200 50  0001 C CNN
	1    1250 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 637DF1A4
P 750 6000
AR Path="/637DF1A4" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/637DF1A4" Ref="#GND070"  Part="1" 
F 0 "#GND070" H 750 5900 50  0001 C CNN
F 1 "0" H 750 6089 50  0000 C CNN
F 2 "" H 750 6000 50  0001 C CNN
F 3 "~" H 750 6000 50  0001 C CNN
	1    750  6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	750  6200 1150 6200
Wire Wire Line
	1350 6200 1750 6200
$Comp
L 74xx:74HC00 U1
U 5 1 637DF1AC
P 1250 6050
F 0 "U1" V 1100 6050 24  0000 C CNN
F 1 "74HC00" V 1150 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1250 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 1250 6050 50  0001 C CNN
	5    1250 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 637DF1B2
P 1750 6050
AR Path="/637DF1B2" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/637DF1B2" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 1750 5900 50  0001 C CNN
F 1 "+5V" H 1765 6223 50  0000 C CNN
F 2 "" H 1750 6050 50  0001 C CNN
F 3 "" H 1750 6050 50  0001 C CNN
	1    1750 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 6200 1750 6050
Connection ~ 1750 6050
Wire Wire Line
	750  6200 750  6050
Wire Wire Line
	750  6000 750  6050
Connection ~ 750  6050
$Comp
L Custom:74HC08 U?
U 4 1 638D769F
P 10400 3800
AR Path="/638D769F" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638D769F" Ref="U8"  Part="4" 
F 0 "U8" H 10350 4050 24  0000 C CNN
F 1 "74HC08" H 10350 4000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10400 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 10400 3800 50  0001 C CNN
	4    10400 3800
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 1 1 638D76A5
P 2750 2150
AR Path="/638D76A5" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638D76A5" Ref="U9"  Part="1" 
F 0 "U9" H 2700 2400 24  0000 C CNN
F 1 "74HC08" H 2700 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2750 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2750 2150 50  0001 C CNN
	1    2750 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 2 1 638D76AB
P 3350 2150
AR Path="/638D76AB" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638D76AB" Ref="U9"  Part="2" 
F 0 "U9" H 3300 2400 24  0000 C CNN
F 1 "74HC08" H 3300 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3350 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 3350 2150 50  0001 C CNN
	2    3350 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 3 1 638D76B1
P 3950 2150
AR Path="/638D76B1" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638D76B1" Ref="U9"  Part="3" 
F 0 "U9" H 3900 2400 24  0000 C CNN
F 1 "74HC08" H 3900 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3950 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 3950 2150 50  0001 C CNN
	3    3950 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 5 1 638D76B7
P 3650 5450
AR Path="/638D76B7" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638D76B7" Ref="U9"  Part="5" 
F 0 "U9" V 3550 5450 24  0000 C CNN
F 1 "74HC08" V 3500 5450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3650 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 3650 5450 50  0001 C CNN
	5    3650 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 638D76BD
P 4150 5450
AR Path="/638D76BD" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/638D76BD" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 4150 5300 50  0001 C CNN
F 1 "+5V" H 4165 5623 50  0000 C CNN
F 2 "" H 4150 5450 50  0001 C CNN
F 3 "" H 4150 5450 50  0001 C CNN
	1    4150 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 638D76C3
P 3150 5400
AR Path="/638D76C3" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/638D76C3" Ref="#GND072"  Part="1" 
F 0 "#GND072" H 3150 5300 50  0001 C CNN
F 1 "0" H 3150 5489 50  0000 C CNN
F 2 "" H 3150 5400 50  0001 C CNN
F 3 "~" H 3150 5400 50  0001 C CNN
	1    3150 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 638D76C9
P 3650 5600
AR Path="/638D76C9" Ref="C?"  Part="1" 
AR Path="/671E6DCB/638D76C9" Ref="C34"  Part="1" 
F 0 "C34" V 3450 5600 24  0000 C CNN
F 1 "100n" V 3500 5600 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3650 5600 50  0001 C CNN
F 3 "~" H 3650 5600 50  0001 C CNN
	1    3650 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 5600 3150 5600
Wire Wire Line
	3750 5600 4150 5600
Wire Wire Line
	4150 5600 4150 5450
Wire Wire Line
	3150 5400 3150 5450
Wire Wire Line
	3150 5450 3150 5600
Connection ~ 3150 5450
Connection ~ 4150 5450
$Comp
L Custom:74HC08 U?
U 4 1 638DF000
P 6250 2150
AR Path="/638DF000" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638DF000" Ref="U9"  Part="4" 
F 0 "U9" H 6200 2400 24  0000 C CNN
F 1 "74HC08" H 6200 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6250 2150 50  0001 C CNN
	4    6250 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 2 1 638DF006
P 9200 2250
AR Path="/638DF006" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638DF006" Ref="U10"  Part="2" 
F 0 "U10" H 9150 2500 24  0000 C CNN
F 1 "74HC08" H 9150 2450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9200 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 9200 2250 50  0001 C CNN
	2    9200 2250
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 4 1 638DF00C
P 1050 3300
AR Path="/638DF00C" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638DF00C" Ref="U10"  Part="4" 
F 0 "U10" H 1000 3550 24  0000 C CNN
F 1 "74HC08" H 1000 3500 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1050 3300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1050 3300 50  0001 C CNN
	4    1050 3300
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 5 1 638DF012
P 4850 5450
AR Path="/638DF012" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638DF012" Ref="U10"  Part="5" 
F 0 "U10" V 4750 5450 24  0000 C CNN
F 1 "74HC08" V 4700 5450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4850 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 4850 5450 50  0001 C CNN
	5    4850 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 638DF018
P 5350 5450
AR Path="/638DF018" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/638DF018" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 5350 5300 50  0001 C CNN
F 1 "+5V" H 5365 5623 50  0000 C CNN
F 2 "" H 5350 5450 50  0001 C CNN
F 3 "" H 5350 5450 50  0001 C CNN
	1    5350 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 638DF01E
P 4350 5400
AR Path="/638DF01E" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/638DF01E" Ref="#GND075"  Part="1" 
F 0 "#GND075" H 4350 5300 50  0001 C CNN
F 1 "0" H 4350 5489 50  0000 C CNN
F 2 "" H 4350 5400 50  0001 C CNN
F 3 "~" H 4350 5400 50  0001 C CNN
	1    4350 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 638DF024
P 4850 5600
AR Path="/638DF024" Ref="C?"  Part="1" 
AR Path="/671E6DCB/638DF024" Ref="C36"  Part="1" 
F 0 "C36" V 4650 5600 24  0000 C CNN
F 1 "100n" V 4700 5600 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4850 5600 50  0001 C CNN
F 3 "~" H 4850 5600 50  0001 C CNN
	1    4850 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 5600 4350 5600
Wire Wire Line
	4950 5600 5350 5600
Wire Wire Line
	5350 5600 5350 5450
Wire Wire Line
	4350 5400 4350 5450
Wire Wire Line
	4350 5450 4350 5600
Connection ~ 4350 5450
Connection ~ 5350 5450
$Comp
L Custom:74HC08 U?
U 1 1 638E6EAC
P 1850 3800
AR Path="/638E6EAC" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638E6EAC" Ref="U11"  Part="1" 
F 0 "U11" H 1800 4050 24  0000 C CNN
F 1 "74HC08" H 1800 4000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1850 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1850 3800 50  0001 C CNN
	1    1850 3800
	0    -1   -1   0   
$EndComp
$Comp
L Custom:74HC08 U?
U 2 1 638E6EB8
P 5150 3400
AR Path="/638E6EB8" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638E6EB8" Ref="U11"  Part="2" 
F 0 "U11" H 5100 3650 24  0000 C CNN
F 1 "74HC08" H 5100 3600 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5150 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5150 3400 50  0001 C CNN
	2    5150 3400
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 3 1 638E6EBE
P 7450 3500
AR Path="/638E6EBE" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638E6EBE" Ref="U11"  Part="3" 
F 0 "U11" H 7400 3750 24  0000 C CNN
F 1 "74HC08" H 7400 3700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7450 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 7450 3500 50  0001 C CNN
	3    7450 3500
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 5 1 638E6EC4
P 6050 5450
AR Path="/638E6EC4" Ref="U?"  Part="1" 
AR Path="/671E6DCB/638E6EC4" Ref="U11"  Part="5" 
F 0 "U11" V 5950 5450 24  0000 C CNN
F 1 "74HC08" V 5900 5450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6050 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6050 5450 50  0001 C CNN
	5    6050 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 638E6ECA
P 6550 5450
AR Path="/638E6ECA" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/638E6ECA" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 6550 5300 50  0001 C CNN
F 1 "+5V" H 6565 5623 50  0000 C CNN
F 2 "" H 6550 5450 50  0001 C CNN
F 3 "" H 6550 5450 50  0001 C CNN
	1    6550 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 638E6ED0
P 5550 5400
AR Path="/638E6ED0" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/638E6ED0" Ref="#GND078"  Part="1" 
F 0 "#GND078" H 5550 5300 50  0001 C CNN
F 1 "0" H 5550 5489 50  0000 C CNN
F 2 "" H 5550 5400 50  0001 C CNN
F 3 "~" H 5550 5400 50  0001 C CNN
	1    5550 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 638E6ED6
P 6050 5600
AR Path="/638E6ED6" Ref="C?"  Part="1" 
AR Path="/671E6DCB/638E6ED6" Ref="C38"  Part="1" 
F 0 "C38" V 5850 5600 24  0000 C CNN
F 1 "100n" V 5900 5600 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 6050 5600 50  0001 C CNN
F 3 "~" H 6050 5600 50  0001 C CNN
	1    6050 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 5600 5550 5600
Wire Wire Line
	6150 5600 6550 5600
Wire Wire Line
	6550 5600 6550 5450
Wire Wire Line
	5550 5400 5550 5450
Wire Wire Line
	5550 5450 5550 5600
Connection ~ 5550 5450
Connection ~ 6550 5450
$Comp
L 74xx:74HC04 U?
U 5 1 639FB99F
P 6550 2550
AR Path="/639FB99F" Ref="U?"  Part="1" 
AR Path="/671E6DCB/639FB99F" Ref="U16"  Part="5" 
F 0 "U16" H 6550 2750 24  0000 C CNN
F 1 "74HC04" H 6550 2700 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6550 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6550 2550 50  0001 C CNN
	5    6550 2550
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 6 1 639FB9AB
P 10100 2650
AR Path="/639FB9AB" Ref="U?"  Part="1" 
AR Path="/671E6DCB/639FB9AB" Ref="U16"  Part="6" 
F 0 "U16" H 10100 2850 24  0000 C CNN
F 1 "74HC04" H 10100 2800 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10100 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 10100 2650 50  0001 C CNN
	6    10100 2650
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 1 1 63A04B7C
P 10850 800
AR Path="/63A04B7C" Ref="U?"  Part="1" 
AR Path="/671E6DCB/63A04B7C" Ref="U16"  Part="1" 
F 0 "U16" H 10850 1000 24  0000 C CNN
F 1 "74HC04" H 10850 950 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10850 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 10850 800 50  0001 C CNN
	1    10850 800 
	1    0    0    1   
$EndComp
$Comp
L 74xx:74HC04 U?
U 3 1 63A04B82
P 4750 7200
AR Path="/63A04B82" Ref="U?"  Part="1" 
AR Path="/671E6DCB/63A04B82" Ref="U19"  Part="3" 
F 0 "U19" H 4750 7400 24  0000 C CNN
F 1 "74HC04" H 4750 7350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4750 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 4750 7200 50  0001 C CNN
	3    4750 7200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 800  1350 700 
Wire Wire Line
	1950 800  1950 700 
Wire Wire Line
	2550 800  2550 700 
Text GLabel 750  700  0    50   Input ~ 0
DBU
Text GLabel 6550 7500 3    50   Input ~ 0
EG
Text GLabel 1950 1500 3    50   Input ~ 0
DDU
Text GLabel 2450 1500 3    50   Input ~ 0
LG
Text GLabel 6100 7500 3    50   Input ~ 0
DDC
Text GLabel 3150 800  2    50   Input ~ 0
~UNS
Wire Wire Line
	4700 800  4700 700 
Text GLabel 4800 1500 3    50   Input ~ 0
~DDC
Text GLabel 4600 1500 3    50   Input ~ 0
LG
Text GLabel 4100 900  3    50   Input ~ 0
~EG
Text GLabel 5650 7500 3    50   Input ~ 0
DDL
Text GLabel 5300 800  2    50   Input ~ 0
~LNR
Text GLabel 5850 800  0    50   Input ~ 0
T2OVF
Wire Wire Line
	6450 800  6450 700 
Text GLabel 7050 800  2    50   Input ~ 0
~ES
Text GLabel 8000 700  0    50   Input ~ 0
~EE
Text GLabel 7300 1400 0    50   Input ~ 0
UE
Text GLabel 7300 1600 0    50   Input ~ 0
E
Text GLabel 8700 1600 2    50   Input ~ 0
UN
Text GLabel 8700 1400 2    50   Input ~ 0
~E
Text GLabel 8600 800  2    50   Input ~ 0
UG
$Comp
L Custom:74HC08 U?
U 3 1 678DF351
P 8300 800
AR Path="/678DF351" Ref="U?"  Part="1" 
AR Path="/671E6DCB/678DF351" Ref="U8"  Part="3" 
F 0 "U8" H 8250 1050 24  0000 C CNN
F 1 "74HC08" H 8250 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8300 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 8300 800 50  0001 C CNN
	3    8300 800 
	1    0    0    -1  
$EndComp
Text GLabel 9850 700  0    50   Input ~ 0
~EE
Text GLabel 9150 1400 0    50   Input ~ 0
LE
Text GLabel 9150 1600 0    50   Input ~ 0
E
Text GLabel 10550 1600 2    50   Input ~ 0
LN
Text GLabel 10550 1400 2    50   Input ~ 0
~E
Text GLabel 11150 800  1    50   Input ~ 0
LG
Text GLabel 1350 2050 2    50   Input ~ 0
~UNR
Text GLabel 750  2150 0    50   Input ~ 0
EG
Text GLabel 750  1950 0    50   Input ~ 0
DDU
$Comp
L 74xx:74HC02 U?
U 3 1 675C828B
P 1050 2050
AR Path="/675C828B" Ref="U?"  Part="3" 
AR Path="/671E6DCB/675C828B" Ref="U15"  Part="3" 
F 0 "U15" H 1050 2300 24  0000 C CNN
F 1 "74HC02" H 1050 2250 24  0000 C CNN
F 2 "" H 1050 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc02" H 1050 2050 50  0001 C CNN
	3    1050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2150 3050 2050
Wire Wire Line
	3650 2150 3650 2050
Wire Wire Line
	4250 2150 4250 2050
Text GLabel 2450 2050 0    50   Input ~ 0
DWL
Text GLabel 2450 2850 3    50   Input ~ 0
UG
Text GLabel 3050 2250 3    50   Input ~ 0
~EG
Text GLabel 3650 2250 3    50   Input ~ 0
~DDL
Text GLabel 4250 2250 3    50   Input ~ 0
DDC
Text GLabel 4850 2150 2    50   Input ~ 0
~LNS
Wire Wire Line
	6550 2150 6550 2050
Text GLabel 5950 2050 0    50   Input ~ 0
~DBU
Text GLabel 5950 2250 0    50   Input ~ 0
~LNS
Text GLabel 6550 2850 3    50   Input ~ 0
T1OVF
Text GLabel 7150 2150 2    50   Input ~ 0
~DWLR
Wire Wire Line
	8300 2250 8300 2150
Wire Wire Line
	8900 2250 8900 2150
Wire Wire Line
	9500 2250 9500 2150
Wire Wire Line
	10100 2250 10100 2150
Text GLabel 10100 2950 3    50   Input ~ 0
T3OVF
Text GLabel 7700 2150 0    50   Input ~ 0
DDL
Text GLabel 5200 7500 3    50   Input ~ 0
EU
Text GLabel 8200 2950 3    50   Input ~ 0
DDU
Text GLabel 8400 2950 3    50   Input ~ 0
EU
Text GLabel 8900 2350 3    50   Input ~ 0
E
Text GLabel 9400 2950 3    50   Input ~ 0
DDU
Text GLabel 9600 2950 3    50   Input ~ 0
DDL
Text GLabel 10950 2250 2    50   Input ~ 0
~EES
Text GLabel 750  2450 0    50   Input ~ 0
LN
Text GLabel 750  2650 0    50   Input ~ 0
UN
Text GLabel 1350 2550 2    50   Input ~ 0
~T2C
Text GLabel 750  3200 0    50   Input ~ 0
E
Text GLabel 750  3400 0    50   Input ~ 0
~EE
Text GLabel 1350 3300 2    50   Input ~ 0
T4C
Text GLabel 1850 3300 0    50   Input ~ 0
DBL
Text GLabel 1750 4100 0    50   Input ~ 0
~DDL
Text GLabel 1950 4100 2    50   Input ~ 0
~UN
Text GLabel 2450 3400 2    50   Input ~ 0
~DWLS
$Comp
L 74xx:74HC00 U5
U 2 1 637AB683
P 3950 3400
F 0 "U5" H 3950 3650 24  0000 C CNN
F 1 "74HC00" H 3950 3600 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3950 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 3950 3400 50  0001 C CNN
	2    3950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3400 4250 3300
Wire Wire Line
	4850 3400 4850 3300
Text GLabel 4750 7500 3    50   Input ~ 0
CB4
Text GLabel 3650 3500 3    50   Input ~ 0
EU
Text GLabel 4150 4100 0    50   Input ~ 0
CB4
Text GLabel 4350 4100 2    50   Input ~ 0
~EU
Text GLabel 4850 3500 3    50   Input ~ 0
CB0
Text GLabel 5450 3400 2    50   Input ~ 0
UE
Wire Wire Line
	6550 3500 6550 3400
Wire Wire Line
	7150 3500 7150 3400
Text GLabel 5950 3400 0    50   Input ~ 0
CB4
Text GLabel 5950 3600 0    50   Input ~ 0
EU
Text GLabel 6450 4200 0    50   Input ~ 0
~CB4
Text GLabel 6650 4200 2    50   Input ~ 0
~EU
Text GLabel 7150 3600 3    50   Input ~ 0
CB0
Text GLabel 7750 3500 2    50   Input ~ 0
LE
Text GLabel 3450 7500 0    50   Input ~ 0
EG
Text GLabel 3500 7500 2    50   Input ~ 0
E
Text GLabel 3800 7500 0    50   Input ~ 0
~ER
Text GLabel 4100 7500 0    50   Input ~ 0
~EER
Text GLabel 6550 6900 1    50   Input ~ 0
~EG
Text GLabel 1350 900  3    50   Input ~ 0
~EG
Text GLabel 6100 6900 1    50   Input ~ 0
~DDC
Text GLabel 2650 1500 3    50   Input ~ 0
~DDC
Text GLabel 5650 6900 1    50   Input ~ 0
~DDL
Text GLabel 4100 700  0    50   Input ~ 0
~DDL
$Comp
L 74xx:74HC04 U?
U 3 1 66F0CDB3
P 6150 800
AR Path="/66F0CDB3" Ref="U?"  Part="1" 
AR Path="/671E6DCB/66F0CDB3" Ref="U16"  Part="3" 
F 0 "U16" H 6150 1000 24  0000 C CNN
F 1 "74HC04" H 6150 950 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6150 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6150 800 50  0001 C CNN
	3    6150 800 
	1    0    0    -1  
$EndComp
Text GLabel 5200 6900 1    50   Input ~ 0
~EU
Text GLabel 7700 2350 0    50   Input ~ 0
~EU
Text GLabel 4750 6900 1    50   Input ~ 0
~CB4
Text GLabel 3650 3300 0    50   Input ~ 0
~CB4
$Comp
L 74xx:74HC00 U1
U 3 1 637C9733
P 4700 1200
F 0 "U1" H 4700 1450 24  0000 C CNN
F 1 "74HC00" H 4700 1400 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4700 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4700 1200 50  0001 C CNN
	3    4700 1200
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC04 U?
U 4 1 639FB9A5
P 5200 7200
AR Path="/639FB9A5" Ref="U?"  Part="1" 
AR Path="/671E6DCB/639FB9A5" Ref="U19"  Part="4" 
F 0 "U19" H 5200 7400 24  0000 C CNN
F 1 "74HC04" H 5200 7350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5200 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5200 7200 50  0001 C CNN
	4    5200 7200
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 642FC243
P 3800 7500
AR Path="/642FC243" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/642FC243" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 3800 7350 50  0001 C CNN
F 1 "+5V" H 3815 7673 50  0000 C CNN
F 2 "" H 3800 7500 50  0001 C CNN
F 3 "" H 3800 7500 50  0001 C CNN
	1    3800 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 642FD259
P 4100 7500
AR Path="/642FD259" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/642FD259" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 4100 7350 50  0001 C CNN
F 1 "+5V" H 4115 7673 50  0000 C CNN
F 2 "" H 4100 7500 50  0001 C CNN
F 3 "" H 4100 7500 50  0001 C CNN
	1    4100 7500
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 3 1 6436A55F
P 2850 800
AR Path="/6436A55F" Ref="U?"  Part="1" 
AR Path="/671E6DCB/6436A55F" Ref="U7"  Part="3" 
F 0 "U7" H 2800 1050 24  0000 C CNN
F 1 "74HC08" H 2800 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2850 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2850 800 50  0001 C CNN
	3    2850 800 
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 1 1 6436D3AB
P 5000 800
AR Path="/6436D3AB" Ref="U?"  Part="1" 
AR Path="/671E6DCB/6436D3AB" Ref="U8"  Part="1" 
F 0 "U8" H 4950 1050 24  0000 C CNN
F 1 "74HC08" H 4950 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5000 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5000 800 50  0001 C CNN
	1    5000 800 
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 2 1 6437F6ED
P 6750 800
AR Path="/6437F6ED" Ref="U?"  Part="1" 
AR Path="/671E6DCB/6437F6ED" Ref="U8"  Part="2" 
F 0 "U8" H 6700 1050 24  0000 C CNN
F 1 "74HC08" H 6700 1000 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6750 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6750 800 50  0001 C CNN
	2    6750 800 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U3
U 3 1 6439E3B0
P 4550 2150
F 0 "U3" H 4550 2400 24  0000 C CNN
F 1 "74HC00" H 4550 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4550 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4550 2150 50  0001 C CNN
	3    4550 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 1 1 643B2CC5
P 6850 2150
AR Path="/643B2CC5" Ref="U?"  Part="1" 
AR Path="/671E6DCB/643B2CC5" Ref="U10"  Part="1" 
F 0 "U10" H 6800 2400 24  0000 C CNN
F 1 "74HC08" H 6800 2350 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6850 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6850 2150 50  0001 C CNN
	1    6850 2150
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 3 1 643C6910
P 10400 2250
AR Path="/643C6910" Ref="U?"  Part="1" 
AR Path="/671E6DCB/643C6910" Ref="U10"  Part="3" 
F 0 "U10" H 10350 2500 24  0000 C CNN
F 1 "74HC08" H 10350 2450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 10400 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 10400 2250 50  0001 C CNN
	3    10400 2250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 1 1 643E9483
P 2150 3400
F 0 "U5" H 2150 3650 24  0000 C CNN
F 1 "74HC00" H 2150 3600 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2150 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2150 3400 50  0001 C CNN
	1    2150 3400
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 5 1 648650EF
P 1250 5450
AR Path="/648650EF" Ref="U?"  Part="1" 
AR Path="/671E6DCB/648650EF" Ref="U7"  Part="5" 
F 0 "U7" V 1150 5450 24  0000 C CNN
F 1 "74HC08" V 1100 5450 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1250 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1250 5450 50  0001 C CNN
	5    1250 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 648650F5
P 1750 5450
AR Path="/648650F5" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/648650F5" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 1750 5300 50  0001 C CNN
F 1 "+5V" H 1765 5623 50  0000 C CNN
F 2 "" H 1750 5450 50  0001 C CNN
F 3 "" H 1750 5450 50  0001 C CNN
	1    1750 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 648650FB
P 750 5400
AR Path="/648650FB" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/648650FB" Ref="#GND074"  Part="1" 
F 0 "#GND074" H 750 5300 50  0001 C CNN
F 1 "0" H 750 5489 50  0000 C CNN
F 2 "" H 750 5400 50  0001 C CNN
F 3 "~" H 750 5400 50  0001 C CNN
	1    750  5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 64865101
P 1250 5600
AR Path="/64865101" Ref="C?"  Part="1" 
AR Path="/671E6DCB/64865101" Ref="C42"  Part="1" 
F 0 "C42" V 1050 5600 24  0000 C CNN
F 1 "100n" V 1100 5600 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1250 5600 50  0001 C CNN
F 3 "~" H 1250 5600 50  0001 C CNN
	1    1250 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 5600 750  5600
Wire Wire Line
	1350 5600 1750 5600
Wire Wire Line
	1750 5600 1750 5450
Wire Wire Line
	750  5400 750  5450
Wire Wire Line
	750  5450 750  5600
Connection ~ 750  5450
Connection ~ 1750 5450
Wire Wire Line
	6450 7500 6650 7500
Wire Wire Line
	3450 7500 3500 7500
$Comp
L Device:R_Small R?
U 1 1 639059B4
P 10800 2250
AR Path="/639059B4" Ref="R?"  Part="1" 
AR Path="/671E6DCB/639059B4" Ref="R49"  Part="1" 
F 0 "R49" V 10850 2150 24  0000 C CNN
F 1 "2k" V 10800 2250 24  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 10800 2250 50  0001 C CNN
F 3 "~" H 10800 2250 50  0001 C CNN
	1    10800 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 2250 10900 2250
$Comp
L Device:C_Small C?
U 1 1 63927104
P 10900 2350
AR Path="/63927104" Ref="C?"  Part="1" 
AR Path="/671E6DCB/63927104" Ref="C44"  Part="1" 
F 0 "C44" V 10700 2350 24  0000 C CNN
F 1 "100n" V 10750 2350 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 10900 2350 50  0001 C CNN
F 3 "~" H 10900 2350 50  0001 C CNN
	1    10900 2350
	1    0    0    -1  
$EndComp
Connection ~ 10900 2250
$Comp
L pspice:0 #GND?
U 1 1 63951ECF
P 10900 2500
AR Path="/63951ECF" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/63951ECF" Ref="#GND088"  Part="1" 
F 0 "#GND088" H 10900 2400 50  0001 C CNN
F 1 "0" H 10900 2589 50  0000 C CNN
F 2 "" H 10900 2500 50  0001 C CNN
F 3 "~" H 10900 2500 50  0001 C CNN
	1    10900 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 2450 10900 2500
$Comp
L Device:C_Small C?
U 1 1 63FC6F2C
P 8450 6200
AR Path="/63FC6F2C" Ref="C?"  Part="1" 
AR Path="/671E6DCB/63FC6F2C" Ref="C47"  Part="1" 
F 0 "C47" V 8250 6200 24  0000 C CNN
F 1 "100n" V 8300 6200 24  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8450 6200 50  0001 C CNN
F 3 "~" H 8450 6200 50  0001 C CNN
	1    8450 6200
	0    -1   -1   0   
$EndComp
$Comp
L pspice:0 #GND?
U 1 1 63FC6F32
P 7950 6000
AR Path="/63FC6F32" Ref="#GND?"  Part="1" 
AR Path="/671E6DCB/63FC6F32" Ref="#GND092"  Part="1" 
F 0 "#GND092" H 7950 5900 50  0001 C CNN
F 1 "0" H 7950 6089 50  0000 C CNN
F 2 "" H 7950 6000 50  0001 C CNN
F 3 "~" H 7950 6000 50  0001 C CNN
	1    7950 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	7950 6200 8350 6200
Wire Wire Line
	8550 6200 8950 6200
$Comp
L 74xx:74HC00 U17
U 5 1 63FC6F3A
P 8450 6050
F 0 "U17" V 8300 6050 24  0000 C CNN
F 1 "74HC00" V 8350 6050 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8450 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8450 6050 50  0001 C CNN
	5    8450 6050
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 63FC6F40
P 8950 6050
AR Path="/63FC6F40" Ref="#PWR?"  Part="1" 
AR Path="/671E6DCB/63FC6F40" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 8950 5900 50  0001 C CNN
F 1 "+5V" H 8965 6223 50  0000 C CNN
F 2 "" H 8950 6050 50  0001 C CNN
F 3 "" H 8950 6050 50  0001 C CNN
	1    8950 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 6200 8950 6050
Connection ~ 8950 6050
Wire Wire Line
	7950 6200 7950 6050
Wire Wire Line
	7950 6000 7950 6050
Connection ~ 7950 6050
$Comp
L 74xx:74HC00 U17
U 1 1 63FD56C5
P 6550 7200
F 0 "U17" H 6550 7450 24  0000 C CNN
F 1 "74HC00" H 6550 7400 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6550 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6550 7200 50  0001 C CNN
	1    6550 7200
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC00 U17
U 3 1 63FED58E
P 8600 3600
F 0 "U17" H 8600 3850 24  0000 C CNN
F 1 "74HC00" H 8600 3800 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8600 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8600 3600 50  0001 C CNN
	3    8600 3600
	1    0    0    -1  
$EndComp
$Comp
L Custom:74HC08 U?
U 4 1 65C816B9
P 9800 3700
AR Path="/65C816B9" Ref="U?"  Part="4" 
AR Path="/671E6DCB/65C816B9" Ref="U11"  Part="4" 
F 0 "U11" H 9750 3950 24  0000 C CNN
F 1 "74HC08" H 9750 3900 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9800 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 9800 3700 50  0001 C CNN
	4    9800 3700
	1    0    0    -1  
$EndComp
Text GLabel 8300 3500 0    50   Input ~ 0
DBU
Text GLabel 8300 3700 0    50   Input ~ 0
DDU
Text GLabel 8900 3600 2    50   Input ~ 0
~SBS
Wire Wire Line
	9350 3800 9500 3800
Wire Wire Line
	9450 3600 9500 3600
Text GLabel 10700 3800 2    50   Input ~ 0
~SBR
Text GLabel 6450 900  3    50   Input ~ 0
~DDU&DDL
Text GLabel 9150 2450 3    50   Input ~ 0
~DDU&DDL
Wire Wire Line
	9500 2350 9350 2350
Wire Wire Line
	9350 2350 9350 2450
Wire Wire Line
	9350 2450 9150 2450
Connection ~ 9500 2350
Text GLabel 9450 3600 0    50   Input ~ 0
~DBL
Wire Wire Line
	9350 4400 9450 4400
Connection ~ 9350 4400
Wire Wire Line
	9350 4450 9350 4400
Text GLabel 9350 4450 3    50   Input ~ 0
T5OVF
Wire Wire Line
	9250 4400 9350 4400
$Comp
L 74xx:74HC00 U17
U 4 1 63FEEF3D
P 9350 4100
F 0 "U17" H 9350 4350 24  0000 C CNN
F 1 "74HC00" H 9350 4300 24  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9350 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 9350 4100 50  0001 C CNN
	4    9350 4100
	0    -1   -1   0   
$EndComp
Text GLabel 10100 3900 3    50   Input ~ 0
DDU
Text GLabel 10500 800  1    50   Input ~ 0
~LG
Wire Wire Line
	10550 800  10450 800 
Text GLabel 750  900  3    50   Input ~ 0
~LG
$EndSCHEMATC
