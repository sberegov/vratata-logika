"use strict";

var choice1 = '<input type="radio" id="in_hi" name="???"> \
<label for="in_hi">HIGH</label> \
<input type="radio" id="in_lo" name="???"> \
<label for="in_lo">LOW</label> \
<input type="radio" id="in_no" name="???" checked="checked"> \
<label for="in_no">NORM</label>';

function appendButton(element, txt){
	var buttonEl = document.createElement("a");
	buttonEl.innerHTML = choice1.replaceAll('???', txt);
	element.appendChild(buttonEl);
}

function appendStateField(element){
	var state = document.createElement("input");
	state.type = "checkbox";
	state.disabled = "true";
	state.id = "state";
	element.appendChild(state)
}

function appendProgress(element){
	var progress = document.createElement("progress");
	progress.value = 0
	progress.max = 100
	element.appendChild(progress)
}

function appendSignalContainer(element){
	var container = document.createElement("div");
	container.id = "container";
	element.appendChild(container);
}

function md_input(evt) {
	var evt_class = evt.currentTarget.parentElement.classList[0]
	processInputClick('md', evt_class);
}

function mu_input(evt) {
	var evt_class = evt.currentTarget.parentElement.classList[0]
	processInputClick('mu', evt_class);
}

document.addEventListener('DOMContentLoaded', (event) => {
	var elements = document.getElementsByClassName('input');
	for(const element of elements){
		appendButton(element, element.classList[0] + 'state');
		element.querySelector('h1').addEventListener('mousedown', md_input);
		element.querySelector('h1').addEventListener('mouseup', mu_input);
		appendStateField(element.querySelector('h1'));
//		console.log(element);
	}
	elements = document.getElementsByClassName('timer');
	for(const element of elements){
		appendProgress(element);
	}
	elements = document.getElementsByClassName('boolean');
	for(const element of elements){
		appendStateField(element.querySelector('h1'));
		appendSignalContainer(element);
	}
	elements = document.getElementsByClassName('counter');
	for(const element of elements){
		element.setAttribute('data-count', '0');
//		element.dataset.count=101;
		appendSignalContainer(element);
		appendStateField(element.querySelector('#container'));
		appendStateField(element.querySelector('#container'));
		appendStateField(element.querySelector('#container'));
		appendStateField(element.querySelector('#container'));
		appendStateField(element.querySelector('#container'));
		appendStateField(element.querySelector('#container'));
	}

	logicEquations();
})

function setState(input_class, state){
	var selector = '.' + input_class + ' #state';
	var elem = document.querySelector(selector)
	state ? elem.setAttribute('checked','') : elem.removeAttribute('checked')
//	setState('DU', 1)
}

function getState(input_class){
	var selector = '.' + input_class + ' #state';
	var elem = document.querySelector(selector)
	return elem.getAttribute('checked') === null ? 0 : 1;
//	getState('DU')
}

function getInputRadio(input_class){
	return document.querySelector('input[name='+input_class+'state]:checked').id;
}

function changeTimerProgress(timer_class, value){
	var selector = '.' + timer_class + ' progress';
	var elem = document.querySelector(selector)
	elem.value = value
//	changeTimerProgress('T1', 50)
}

function setBooleanSignals(boolean_class, signals){
	var selector = '.' + boolean_class + ' #container';
	var elem = document.querySelector(selector)
	while (elem.firstChild) elem.removeChild(elem.firstChild);
	for(const signal in signals){
		var sig = document.createElement("a");
		sig.text = signal;
		sig.id = signals[signal] ? 'H' : 'L';
		elem.appendChild(sig);
	}
//	setBooleanSignals('DDU', {'T1' : 1, 'T3' : 0});
}

function updateCounter(counter_class){
	for(let i = 0; i < 6; ++i){
	    var hasbit = parseInt(document.querySelector('.' + counter_class).dataset.count) & (1<<i);
	    var child = document.querySelector('.'+ counter_class + ' #container').children[5-i];
	    hasbit ? child.setAttribute('checked','') : child.removeAttribute('checked');
	}
}

function incrementCounter(counter_class){
	var selector = '.' + counter_class;
	var elem = document.querySelector(selector);
	elem.dataset.count++;
//	incrementCounter('C');
	updateCounter(counter_class);
}

function clearCounter(counter_class){
	var selector = '.' + counter_class;
	var elem = document.querySelector(selector);
	elem.dataset.count = 0;
//	clearCounter('C');
	updateCounter(counter_class);
}

function getCounter(counter_class){
	var selector = '.' + counter_class;
	var elem = document.querySelector(selector);
	return elem.dataset.count;
//	getCounter('C')
}
